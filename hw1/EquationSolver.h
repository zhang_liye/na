#include<iostream>
#include<functional>
#include<cmath>
using namespace std;

#define Epsilon 0.0000001

class EquationSolver
{
public:
    EquationSolver(const function<double(const double&)>& a) : funct(a) {};
    double value(const double& x) const //函数取值
    {
        return funct(x);
    }
    double derivate(const double& x) const //函数取导数值
    {
        return (funct(x)-funct(x-Epsilon))/Epsilon;
    }
    virtual void solve() = 0;
private:
    function<double(const double&)> funct;
};

class Bisection: public EquationSolver
{
public:
    Bisection(const function<double(const double&)>& _func, int _m, double _a, double _b, double _delta, double _epsilon) :
    EquationSolver(_func), M(_m), a(_a), b(_b), delta(_delta), epsilon(_epsilon) {};
    void solve()
    {
        double h = b - a;
        double u = value(a);
        int k = 1;
        for(; k <= M; k++)
        {
            h = h / 2;
            double c = a + h;
            if (abs(h) < delta || k == M)
            {break;}
            double w = value(c);
            if (abs(w) < epsilon)
            {break;}
            else if (w * u > 0)
            {a = c;}
        }
        M = k;
    }
    double value_root()
    {
        return a;
    }
    int value_count()
    {
        return M;
    }
private:
    int M;
    double a;
    double b;
    double delta;
    double epsilon;
};

class Newton: public EquationSolver
{
public:
    Newton(const function<double(const double&)>& _func, int _m, double _x0, double _epsilon) :
    EquationSolver(_func), M(_m), x0(_x0), epsilon(_epsilon) {};
    void solve()
    {
        double x = x0;
        int k = 1;
        for(; k <= M; k++)
        {
            double u = value(x);
            if (abs(u) < epsilon)
            {break;}
            x = x - u/derivate(x);
        }
        x0 = x;
        M = k;
    }
    double value_root()
    {
        return x0;
    }
    int value_count()
    {
        return M;
    }
private:
    int M;
    double x0;
    double epsilon;
};

class Secant: public EquationSolver
{
public:
    Secant(const function<double(const double&)>& _func, int _m, double _x0, double _x1, double _delta, double _epsilon) :
    EquationSolver(_func), M(_m), x0(_x0), x1(_x1), delta(_delta), epsilon(_epsilon) {};
    void solve()
    {
        double xn = x1;
        double xm = x0;
        double u = value(xn);
        double v = value(xm);
        double s;
        int k = 2;
        for(;k<M;k++)
        {
            s = (xn - xm)/(u - v);
            xm = xn;
            v = u;
            xn -= (u*s);
            if(abs(xn-xm) < delta)
            {break;}
            u = value(xn);
            if(abs(u) < epsilon)
            {break;}
        }
        x1 = xn;
        M  = k;
    }
    double value_root()
    {
        return x1;
    }
    int value_count()
    {
        return M;
    }
private:
    int M;
    double x0;
    double x1;
    double delta;
    double epsilon;
};