#include"EquationSolver.h"
#define pi 3.14159265
#define e  2.71828183
#define eps 1e-6
#define del 1e-6
using namespace std;

double func_B1(double x)
{
    return 1 / x - tan(x);
}
double func_B2(double x)
{
    return 1/x - pow(2,x);
}
double func_B3(double x)
{
    return pow(2,-x) + pow(e,x) + 2*cos(x) - 6;
}
double func_B4(double x)
{
    return (pow(x,3) + 4*x*x + 3*x + 5)/(2*pow(x,3) - 9*x*x + 18*x - 2);
}
double func_C(double x)
{
    return x - tan(x);
}
double func_D1(double x)
{
    return sin(x/2) - 1;
}
double func_D2(double x)
{
    return pow(e,x) - tan(x);
}
double func_D3(double x)
{
    return pow(x,3) -12*x*x + 3*x + 1;
}
double func_E(double x)
{
    return 10*(0.5*pi - asin(x) - x*pow(1-x*x,1/2)) - 12.4;
}
double func_F1(double x)
{
    return 17.743752*sin(x)*cos(x) + 87.213325*sin(x)*sin(x) - 9.656722*cos(x) - 47.4642625*sin(x);
}
double func_F2(double x)
{
    return 17.743752*sin(x)*cos(x) + 87.213325*sin(x)*sin(x) - 9.707772*cos(x) - 47.7152*sin(x);
}

int main()
{
    Bisection B_1(func_B1, 100, 0, pi/2, del, eps);
    B_1.solve();
    cout << "Problem B (1)" << endl;
    cout << "root = " << B_1.value_root() << endl;
    cout << "iteration = " << B_1.value_count() << endl;
    cout << "f(root) = " << func_B1(B_1.value_root()) << endl;
    cout << endl;

    Bisection B_2(func_B2, 100, 0, 1, del, eps);
    B_2.solve();
    cout << "Problem B (2)" << endl;
    cout << "root = " << B_2.value_root() << endl;
    cout << "iteration = " << B_2.value_count() << endl;
    cout << "f(root) = " << func_B2(B_2.value_root()) << endl;
    cout << endl;

    Bisection B_3(func_B3, 100, 1, 3, del, eps);
    B_3.solve();
    cout << "Problem B (3)" << endl;
    cout << "root = " << B_3.value_root() << endl;
    cout << "iteration = " << B_3.value_count() << endl;
    cout << "f(root) = " << func_B3(B_3.value_root()) << endl;
    cout << endl;

    Bisection B_4(func_B4, 100, 0, 4, del, eps);
    B_4.solve();
    cout << "Problem B (4)" << endl;
    cout << "root = " << B_4.value_root() << endl;
    cout << "iteration = " << B_4.value_count() << endl;
    cout << "f(root) = " << func_B4(B_4.value_root()) << endl;
    cout << endl;

    Newton C_1(func_C, 100, 4.5, eps);
    C_1.solve();
    cout << "Problem C (1)" << endl;
    cout << "root = " << C_1.value_root() << endl;
    cout << "iteration = " << C_1.value_count() << endl;
    cout << "f(root) = " << func_C(C_1.value_root()) << endl;
    cout << endl;

    Newton C_2(func_C, 100, 7.7, eps);
    C_2.solve();
    cout << "Problem C (2)" << endl;
    cout << "root = " << C_2.value_root() << endl;
    cout << "iteration = " << C_2.value_count() << endl;
    cout << "f(root) = " << func_C(C_2.value_root()) << endl;
    cout << endl;

    Secant D_1(func_D1, 100, 0, pi/2, del, eps);
    D_1.solve();
    cout << "Problem D (1)" << endl;
    cout << "root = " << D_1.value_root() << endl;
    cout << "iteration = " << D_1.value_count() << endl;
    cout << "f(root) = " << func_D1(D_1.value_root()) << endl;
    cout << endl;

    Secant D_2(func_D2, 100, 1, 1.4, del, eps);
    D_2.solve();
    cout << "Problem D (2)" << endl;
    cout << "root = " << D_2.value_root() << endl;
    cout << "iteration = " << D_2.value_count() << endl;
    cout << "f(root) = " << func_D2(D_2.value_root()) << endl;
    cout << endl;

    Secant D_3(func_D3, 100, 0, -0.5, del, eps);
    D_3.solve();
    cout << "Problem D (3)" << endl;
    cout << "root = " << D_3.value_root() << endl;
    cout << "iteration = " << D_3.value_count() << endl;
    cout << "f(root) = " << func_D3(D_3.value_root()) << endl;
    cout << endl;

    Secant D_4(func_D3, 100, 10, 11, del, eps);
    D_4.solve();
    cout << "Problem D (3) with the initial values x0=10, x1=11" << endl;
    cout << "root = " << D_4.value_root() << endl;
    cout << "iteration = " << D_4.value_count() << endl;
    cout << "f(root) = " << func_D3(D_4.value_root()) << endl;
    cout << endl;
    
    Bisection E_1(func_E, 100, 0, 1, del, eps);
    E_1.solve();
    cout << "Problem E with bisection" << endl;
    cout << "root = " << E_1.value_root() << endl;
    cout << "iteration = " << E_1.value_count() << endl;
    cout << "f(root) = " << func_E(E_1.value_root()) << endl;
    cout << endl;

    Newton E_2(func_E, 100, 0.5, eps);
    E_2.solve();
    cout << "Problem E with newton" << endl;
    cout << "root = " << E_2.value_root() << endl;
    cout << "iteration = " << E_2.value_count() << endl;
    cout << "f(root) = " << func_E(E_2.value_root()) << endl;
    cout << endl;

    Secant E_3(func_E, 100, 0, 1, del, eps);
    E_3.solve();
    cout << "Problem E with secant" << endl;
    cout << "root = " << E_3.value_root() << endl;
    cout << "iteration = " << E_3.value_count() << endl;
    cout << "f(root) = " << func_E(E_3.value_root()) << endl;
    cout << endl;

    Newton F_1(func_F1, 100, 0.5759, eps);
    F_1.solve();
    cout << "Problem F (a)" << endl;
    cout << "root = " << F_1.value_root() << endl;
    cout << "iteration = " << F_1.value_count() << endl;
    cout << "f(root) = " << func_F1(F_1.value_root()) << endl;
    cout << endl;

    Newton F_2(func_F2, 100, 0.5759, eps);
    F_2.solve();
    cout << "Problem F (b)" << endl;
    cout << "root = " << F_2.value_root() << endl;
    cout << "iteration = " << F_2.value_count() << endl;
    cout << "f(root) = " << func_F2(F_2.value_root()) << endl;
    cout << endl;

    Secant F_3(func_F1, 100, 3, 4, del, eps);
    F_3.solve();
    cout << "Problem F (c)" << endl;
    cout << "root = " << F_3.value_root() << endl;
    cout << "iteration = " << F_3.value_count() << endl;
    cout << "f(root) = " << func_F1(F_3.value_root()) << endl;
    cout << endl;
}