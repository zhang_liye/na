load("Bdata.mat")
X = Bdata(:,1);
Realvalue = Bdata(:,2);
Func1 = Bdata(:,3);
Func2 = Bdata(:,4);
Func3 = Bdata(:,5);
Func4 = Bdata(:,6);

figure(1)
plot(X, Realvalue, 'r', X, Func1, 'b', ...
     X, Func2, 'g', X, Func3, 'k', ...
     X, Func4, 'y');
grid on
str = {'Realvalue','Func1','Func2','Func3','Func4'};
legend(str,'FontSize',15);
title('Bplot');