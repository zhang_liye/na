load("Cdata.mat")
X = Cdata(:,1);
Realvalue = Cdata(:,2);
Func1 = Cdata(:,3);
Func2 = Cdata(:,4);
Func3 = Cdata(:,5);
Func4 = Cdata(:,6);

figure(1)
plot(X, Realvalue, 'r', X, Func1, 'b', ...
     X, Func2, 'g', X, Func3, 'k', ...
     X, Func4, 'y');
grid on
str = {'Realvalue','Func1','Func2','Func3','Func4'};
legend(str,'FontSize',15);
title('Cplot');