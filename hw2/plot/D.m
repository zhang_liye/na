load("Ddata.mat")
Time = Ddata(:,1);
Position = Ddata(:,2);
Speed = Ddata(:,3);
n = size(Time,1);
Speedline = 81 * ones(n, 1);

subplot(2,1,1)
plot(Time, Position);
grid on
legend('Postion','FontSize',15);
xlabel('Time');
ylabel('Position')

subplot(2,1,2)
plot(Time, Speed, 'r.-', Time, Speedline, 'b.:');
grid on
str = {'Speed','Speed = 81'};
legend(str,'FontSize',15);
xlabel('Time');
ylabel('Speed')