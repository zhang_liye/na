load("Edata.mat")
Day = Edata(:,1);
Sp1 = Edata(:,2);
Sp2 = Edata(:,3);

figure(1)
plot(Day, Sp1, 'r', Day, Sp2, 'b')
grid on
str = {'Sp1','Sp2'};
legend(str,'FontSize',15);
title('Eplot');