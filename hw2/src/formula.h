#include<iostream>
#include<vector>
#include<cmath>

using namespace std;

class formula
{
protected:
    vector<double> x,y;
    vector<vector<double>> f;
public:
    formula(const vector<double> &_x, const vector<double> &_y): x(_x), y(_y) {};
    virtual double calculate(const double X) = 0;
};

class newton : public formula
{
public:
    newton(const vector<double> &_x, const vector<double> &_y): formula(_x,_y) {};
    virtual double calculate(const double X)
    {
        int n = x.size();
        double ans = 0;

        f.resize(n);
        for (int i=0;i<n;i++)
	    {
	        f[i].resize(n);
        } //定义二维数组f大小

        for(int i = 0; i < n; i++)
        {
            f[0][i] = y[i];
        } //初始化二维数组f

        for(int i = 1; i < n; i++)
        {
            for(int j = i; j < n; j++)
            {
                f[i][j] = (f[i-1][j] - f[i-1][j-1]) / (x[j]-x[j-i]);
            }
        } //计算二维数组f

        for(int i = 0; i < n; i++)
        {
            double tmp=f[i][i];
            for (int j = 0; j < i; j++)
            {
                tmp = tmp * (X - x[j]);
            }
            ans += tmp;
        } //代入X计算p(X)

        return ans;
    }
};

class Hermite: public formula
{
protected:
    vector<double> dy;
public:
    Hermite(const vector<double> &_x, const vector<double> &_y, const vector<double> &_dy): formula(_x,_y),dy(_dy) {};
    virtual double calculate(const double X)
    {
        int n = x.size();
        double ans = 0;

        f.resize(n);
        for (int i=0;i<n;i++)
	    {
	        f[i].resize(n);
        } //定义二维数组f大小

        for(int i = 0; i < n; i++)
        {
            f[0][i] = y[i];
        } //初始化二维数组f

        for(int i = 1; i < n; i++)
        {
            for(int j = i; j < n; j++)
            {
                if(x[j] == x[j-i])
                {
                    f[i][j] = dy[j];
                }
                else
                {
                    f[i][j] = (f[i-1][j] - f[i-1][j-1]) / (x[j]-x[j-i]);
                }
            }
        } //计算二维数组f

        for(int i = 0; i < n; i++)
        {
            double tmp=f[i][i];
            for (int j = 0; j < i; j++)
            {
                tmp = tmp * (X - x[j]);
            }
            ans += tmp;
        } //代入X计算p(X)

        return ans;
    }
};