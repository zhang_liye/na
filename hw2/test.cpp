#include <algorithm>
#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include "src/formula.h"
#define pi 3.1415927
#define epsilon 1e-8
using namespace std;

double funcB(double x)
{
    return 1.0 / (1.0 + pow(x, 2));
}
double funcC(double x)
{
    return 1.0 / (1.0 + 25.0 * pow(x, 2));
}

void Bsolution()
{
    vector<newton> B;
    for(int n = 2; n <= 8; n += 2)
    {
        vector<double> x, y;
        for (int i = 0; i <= n; i++)
            {
                x.push_back(-5.0 + 10.0 * i/n);
                y.push_back(funcB(x[i]));
            }
        newton Newton(x, y);
        B.push_back(Newton);
    }

    ofstream Bresult;
    Bresult.open("result/B.txt");

    Bresult << "X" << " : " << "真实值" << " , " << "funcB1(X)" << " , " << "funcB2(X)" << " , " << "funcB3(X)" << " , " << "funcB4(X)" << endl;
    
    for (double X = -5; X <= 5; X += 0.01)
        {
            Bresult << X << " : " << funcB(X) << " , " << B[0].calculate(X) << " , " << B[1].calculate(X) << " , " << B[2].calculate(X) << " , " << B[3].calculate(X) << endl;
        }
    
    Bresult.close();

    cout << "The answer of Question B is displayed in result/B.txt." << endl;
}

void Csolution()
{
    vector<newton> C;
    for(int n = 5; n <= 20; n += 5)
    {
        vector<double> x, y;
        for (int i = 0; i < n; i++)
            {
                x.push_back(cos((2 * i + 1) * pi / (2 * n)));
                y.push_back(funcC(x[i]));
            }
        newton Newton(x, y);
        C.push_back(Newton);
    }

    ofstream Cresult;
    Cresult.open("result/C.txt");

    Cresult << "X" << " : " << "真实值" << " , " << "funcC1(X)" << " , " << "funcC2(X)" << " , " << "funcC3(X)" << " , " << "funcC4(X)" << endl;
    
    for (double X = -1; X <= 1; X += 0.01)
        {
            Cresult << X << " : " << funcC(X) << " , " << C[0].calculate(X) << " , " << C[1].calculate(X) << " , " << C[2].calculate(X) << " , " << C[3].calculate(X) << endl;
        }
    
    Cresult.close();

    cout<<"The answer of Question C is displayed in result/C.txt."<<endl;
}

void Dsolution()
{
    vector<double> Time={0, 0, 3, 3, 5, 5, 8, 8, 13, 13};
    vector<double> displacement={0, 0, 225, 225, 383, 383, 623, 623, 993, 993};
    vector<double> velocity={75, 75, 77, 77, 80, 80, 74, 74, 72, 72};

    Hermite funcD(Time, displacement, velocity);

    cout << "question(a)" << endl;
    double position = funcD.calculate(10);
    double _position = funcD.calculate(10 + epsilon);
    double speed = (_position - position) / epsilon;
    cout<< "Position = " << position << endl;
    cout<< "Speed = " << speed << endl;

    ofstream Dresult;
    Dresult.open("result/D.txt");

    Dresult << "Time" << " : " << "Position" << " , " << "Speed" <<endl;

    int flag = 0;
    for (double X = 0; X <= 13; X += 0.01)
        {
            double position = funcD.calculate(X);
            double _position = funcD.calculate(X + epsilon);
            double speed = (_position - position) / epsilon;
            Dresult << X << " : " << position << " , " << speed <<endl;
            if (speed > 81)
            {
                flag = 1;
            }
        }
    
    Dresult.close();

    cout << "question(b)" << endl;

    cout << "The answer of Question D is displayed in result/D.txt." << endl;
    cout << "And the conclusion is that the car ";
    if (flag)
    {
        cout << "exceeds the speed limit." << endl;
    }
    else
    {
        cout << "doesn't exceed the speed limit." << endl;
    }
}

void Esolution()
{
    vector<double> Day, Sp1, Sp2;
    Day={0, 6, 10, 13, 17, 20, 28};
    Sp1={6.67, 17.3, 42.7, 37.3, 30.1, 29.3, 28.7};
    Sp2={6.67, 16.1, 18.9, 15.0, 10.6, 9.44, 8.89};

    newton funcE1(Day, Sp1);
    newton funcE2(Day, Sp2);

    ofstream Eresult;
    Eresult.open("result/E.txt");

    Eresult << "Day" << " : " << "Sp1" << " , " << "Sp2" <<endl;

    for (double X = 0; X <= 28; X += 0.01)
        {
            Eresult << X << " : " << funcE1.calculate(X) << " , " << funcE2.calculate(X) << endl;
        }
    
    Eresult.close();

    cout << "qestion(a)" << endl;
    cout << "The answer of Question E(a) is displayed in result/E.txt." << endl;

    cout << "qestion(b)" << endl;
    double sp1 = funcE1.calculate(43);
    double sp2 = funcE2.calculate(43);
    cout << "Sp1's weight is "<< sp1 << ", which shows that it is ";
    if (sp1 > 0)
        {
            cout << "not dead." << endl;
        }
    else
        {
            cout << "dead." << endl;
        }
    cout << "Sp2's weight is "<< sp2 << ", which shows that it is ";
    if (sp2 > 0)
        {
            cout << "not dead." << endl;
        }
    else
        {
            cout << "dead." << endl;
        }
    cout << "The result shows that Newton's forumula is not applicable to this question due to the Runge phenomenon." << endl;
}

int main()
{
    cout << "QB" << endl;
    Bsolution();
    cout << "-----------------------------------------------------------" << endl;
    cout << "QC" << endl;
    Csolution();
    cout << "-----------------------------------------------------------" << endl;
    cout << "QD" << endl;
    Dsolution();
    cout << "-----------------------------------------------------------" << endl;
    cout << "QE" << endl;
    Esolution();
    return 0;
}