#include "src/Spline.h"
using namespace std;

double f(double x)
{
    return 1.0 / (1 + 5 * x * x);
}

int main()
{
    std::ofstream outputFile("Bsplinetest.txt");
    int i, j, k;
    double t;
    char color[5] = {'y', 'r', 'g', 'b', 'm'};
    int N1 = 21;
    vector<double> maxnorm(5,0);
    outputFile << "x=-2:0.001:2;" << endl;
    outputFile << "f=1.0./(1 + 5 .* x .* x);" << endl;
    outputFile << "plot(x,f,'k');" << endl;
    outputFile << "hold on;" << endl;
    vector<double> points(N1, 0);
    vector<vector<double>> F(2, vector<double>(N1, 0));
    for (i = 0; i < N1; i++)
    {
        points[i] = -2 + 4.0 / (N1 - 1) * i;
        F[0][i] = f(points[i]);
    }
    F[1][0] = 20.0 / 21 / 21;
    F[1][N1 - 1] = -20.0 / 21 / 21;
    vector<int> M(N1, 0);
    InterpConditions condition = {N1, points, M, F};

    outputFile << "% Ord = 2, Type = Bspline" << endl;
    int Controln1 = N1 + 2 - 3;
    vector<double> controlpoints1(Controln1, 0);
    vector<double> controlvalue1(Controln1, 0);
    for (i = 0; i < Controln1; i++)
    {
        controlpoints1[i] = -2 + 4.0 / (Controln1 - 1) * i;
        controlvalue1[i] = f(controlpoints1[i]);
    }
    Controlpoint control1 = {controlpoints1, controlvalue1};
    Spline<1, 2, BForm> Sp1(N1);
    Sp1 = interpolate_BForm<2>(condition, control1);
    for (i = 0; i < N1 - 1; i++)
    {
        outputFile << "x=" << points[i] << ":0.001:" << points[i + 1] << ";" << endl;
        outputFile << "y_" << N1 << "_" << i << "=";
        streambuf *originalCoutStreamBuffer = cout.rdbuf();
        cout.rdbuf(outputFile.rdbuf());
        Sp1.data_double[i].display();
        cout.rdbuf(originalCoutStreamBuffer);
        outputFile << "plot(x,y_" << N1 << "_" << i << ",'" << color[0] << "'"
                    << ");" << endl;
        outputFile << "hold on;" << endl;
    }
    outputFile << endl;

    outputFile << "% Ord = 3, Type = Bspline" << endl;
    int Controln2 = N1 + 3 - 3;
    vector<double> controlpoints2(Controln2, 0);
    vector<double> controlvalue2(Controln2, 0);
    for (i = 0; i < Controln2; i++)
    {
        controlpoints2[i] = -2 + 4.0 / (Controln2 - 1) * i;
        controlvalue2[i] = f(controlpoints2[i]);
    }
    Controlpoint control2 = {controlpoints2, controlvalue2};
    Spline<1, 3, BForm> Sp2(N1);
    Sp2 = interpolate_BForm<3>(condition, control2);
    for (i = 0; i < N1 - 1; i++)
    {
        outputFile << "x=" << points[i] << ":0.001:" << points[i + 1] << ";" << endl;
        outputFile << "y_" << N1 << "_" << i << "=";
        streambuf *originalCoutStreamBuffer = cout.rdbuf();
        cout.rdbuf(outputFile.rdbuf());
        Sp2.data_double[i].display();
        cout.rdbuf(originalCoutStreamBuffer);
        outputFile << "plot(x,y_" << N1 << "_" << i << ",'" << color[1] << "'"
                    << ");" << endl;
        outputFile << "hold on;" << endl;
    }
    outputFile << endl;

    outputFile << "% Ord = 4, Type = Bspline" << endl;
    int Controln3 = N1 + 4 - 3;
    vector<double> controlpoints3(Controln3, 0);
    vector<double> controlvalue3(Controln3, 0);
    for (i = 0; i < Controln3; i++)
    {
        controlpoints3[i] = -2 + 4.0 / (Controln3 - 1) * i;
        controlvalue3[i] = f(controlpoints3[i]);
    }
    Controlpoint control3 = {controlpoints3, controlvalue3};
    Spline<1, 4, BForm> Sp3(N1);
    Sp3 = interpolate_BForm<4>(condition, control3);
    for (i = 0; i < N1 - 1; i++)
    {
        outputFile << "x=" << points[i] << ":0.001:" << points[i + 1] << ";" << endl;
        outputFile << "y_" << N1 << "_" << i << "=";
        streambuf *originalCoutStreamBuffer = cout.rdbuf();
        cout.rdbuf(outputFile.rdbuf());
        Sp3.data_double[i].display();
        cout.rdbuf(originalCoutStreamBuffer);
        outputFile << "plot(x,y_" << N1 << "_" << i << ",'" << color[2] << "'"
                    << ");" << endl;
        outputFile << "hold on;" << endl;
    }
    outputFile << endl;

    outputFile << "% Ord = 5, Type = Bspline" << endl;
    int Controln4 = N1 + 5 - 3;
    vector<double> controlpoints4(Controln4, 0);
    vector<double> controlvalue4(Controln4, 0);
    for (i = 0; i < Controln4; i++)
    {
        controlpoints4[i] = -2 + 4.0 / (Controln4 - 1) * i;
        controlvalue4[i] = f(controlpoints4[i]);
    }
    Controlpoint control4 = {controlpoints4, controlvalue4};
    Spline<1, 5, BForm> Sp4(N1);
    Sp4 = interpolate_BForm<5>(condition, control4);
    for (i = 0; i < N1 - 1; i++)
    {
        outputFile << "x=" << points[i] << ":0.001:" << points[i + 1] << ";" << endl;
        outputFile << "y_" << N1 << "_" << i << "=";
        streambuf *originalCoutStreamBuffer = cout.rdbuf();
        cout.rdbuf(outputFile.rdbuf());
        Sp4.data_double[i].display();
        cout.rdbuf(originalCoutStreamBuffer);
        outputFile << "plot(x,y_" << N1 << "_" << i << ",'" << color[3] << "'"
                    << ");" << endl;
        outputFile << "hold on;" << endl;
    }
    outputFile << endl;

    outputFile << "% Ord = 6, Type = Bspline" << endl;
    int Controln5 = N1 + 6 - 3;
    vector<double> controlpoints5(Controln5, 0);
    vector<double> controlvalue5(Controln5, 0);
    for (i = 0; i < Controln5; i++)
    {
        controlpoints5[i] = -2 + 4.0 / (Controln5 - 1) * i;
        controlvalue5[i] = f(controlpoints5[i]);
    }
    Controlpoint control5 = {controlpoints5, controlvalue5};
    Spline<1, 6, BForm> Sp5(N1);
    Sp5 = interpolate_BForm<6>(condition, control5);
    for (i = 0; i < N1 - 1; i++)
    {
        outputFile << "x=" << points[i] << ":0.001:" << points[i + 1] << ";" << endl;
        outputFile << "y_" << N1 << "_" << i << "=";
        streambuf *originalCoutStreamBuffer = cout.rdbuf();
        cout.rdbuf(outputFile.rdbuf());
        Sp5.data_double[i].display();
        cout.rdbuf(originalCoutStreamBuffer);
        outputFile << "plot(x,y_" << N1 << "_" << i << ",'" << color[4] << "'"
                    << ");" << endl;
        outputFile << "hold on;" << endl;
    }
    outputFile << endl;
    outputFile.close();
}
