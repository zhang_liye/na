#include "src/Spline.h"

using namespace std;

double f(double x)
{
    return 1.0 / (1 + 25 * x * x);
}

int main()
{
    std::ofstream outputFile("Convergencetest.txt");
    int i, j, k;
    double t;
    int inputN[5] = {25, 50, 100, 200, 400};
    char color[5] = {'y', 'r', 'g', 'b', 'm'};
    vector<vector<double>> maxnorm(2, vector<double>(5, 0));

    // complete --------------------------------------------

    for (k = 0; k < 5; k++)
    {
        int N1 = inputN[k];
        vector<double> points(N1, 0);
        vector<vector<double>> F(2, vector<double>(N1, 0));
        for (i = 0; i < N1; i++)
        {
            points[i] = -1 + 2.0 / (N1 - 1) * i;
            F[0][i] = f(points[i]);
        }
        F[1][0] = 50.0 / 26 / 26;
        F[1][N1 - 1] = -50.0 / 26 / 26;
        vector<int> M(N1, 0);
        InterpConditions condition = {N1, points, M, F};
        Spline<1, 3, ppForm> Sp1(N1);
        Sp1 = interpolate_ppForm<3>(condition, complete);

        // 计算maxnorm
        double max = 0;
        for (i = 0; i < N1 - 1; i++)
        {
            double xx = points[i] + 1.0 / (N1 - 1);
            double tmpd = abs(f(xx) - Sp1.data_double[i](xx));
            if (tmpd > max)
            {
                max = tmpd;
            }
        }
        maxnorm[0][k] = max;
    }
    
    // linear --------------------------------------------

    for (k = 0; k < 5; k++)
    {
        int N1 = inputN[k];
        vector<double> points(N1, 0);
        vector<vector<double>> F(2, vector<double>(N1, 0));
        for (i = 0; i < N1; i++)
        {
            points[i] = -1 + 2.0 / (N1 - 1) * i;
            F[0][i] = f(points[i]);
        }
        F[1][0] = 50.0 / 26 / 26;
        F[1][N1 - 1] = -50.0 / 26 / 26;
        vector<int> M(N1, 0);
        InterpConditions condition = {N1, points, M, F};
        Spline<1, 1, ppForm> Sp1(N1);
        Sp1 = interpolate_ppForm<1>(condition, complete);
        // 计算maxnorm
        double max = 0;
        for (i = 0; i < N1 - 1; i++)
        {
            double xx = points[i] + 1.0 / (N1 - 1);
            double tmpd = abs(f(xx) - Sp1.data_double[i](xx));
            if (tmpd > max)
            {
                max = tmpd;
            }
        }
        maxnorm[1][k] = max;
    }
    double a = 0;
    double b = 0;
    for(int i = 0; i < 4; i++)
    {
        a += maxnorm[0][i]/maxnorm[0][i+1];
        b += maxnorm[1][i]/maxnorm[1][i+1];
    }
    a /= 4;
    b /= 4;

    outputFile << "maxnorm:" << endl;
    outputFile << "pointnumber               25            50            100             200              400" << endl;
    outputFile << "complete: ";
    for (i = 0; i < 5; i++)
    {
        outputFile << "     " << maxnorm[0][i];
    }
    outputFile << endl;
    outputFile << "linear: ";
        for (i = 0; i < 5; i++)
    {
        outputFile << "     " << maxnorm[1][i];
    }
    outputFile << endl;
    outputFile << "Convergence Order of complete:" << a << endl;
    outputFile << "Convergence Order of linear:" << b << endl;

    outputFile.close();

    return 0;
}