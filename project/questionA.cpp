#include "src/Spline.h"

using namespace std;

double f(double x)
{
    return 1.0 / (1 + 25 * x * x);
}

int main()
{
    std::ofstream outputFile("questionA.txt");
    int i, j, k;
    double t;
    int inputN[5] = {6, 11, 21, 41, 81};
    char color[5] = {'y', 'r', 'g', 'b', 'm'};
    int kk[5] = {2, 4, 9, 19, 39};
    vector<vector<double>> maxnorm(4, vector<double>(5, 0));

    // complete --------------------------------------------
    outputFile << "x=-1:0.001:1;" << endl;
    outputFile << "f=1.0./(1 + 25 .* x .* x);" << endl;
    outputFile << "plot(x,f,'k');" << endl;
    outputFile << "hold on;" << endl;

    for (k = 0; k < 5; k++)
    {
        int N1 = inputN[k];
        outputFile << "% N = " << N1 << ", Type = complete" << endl;
        vector<double> points(N1, 0);
        vector<vector<double>> F(2, vector<double>(N1, 0));
        for (i = 0; i < N1; i++)
        {
            points[i] = -1 + 2.0 / (N1 - 1) * i;
            F[0][i] = f(points[i]);
        }
        F[1][0] = 50.0 / 26 / 26;
        F[1][N1 - 1] = -50.0 / 26 / 26;
        vector<int> M(N1, 0);
        InterpConditions condition = {N1, points, M, F};
        Spline<1, 3, ppForm> Sp1(N1);
        Sp1 = interpolate_ppForm<3>(condition, complete);
        for (i = 0; i < N1 - 1; i++)
        {
            outputFile << "x=" << points[i] << ":0.001:" << points[i + 1] << ";" << endl;
            outputFile << "y_" << N1 << "_" << i << "=";
            streambuf *originalCoutStreamBuffer = cout.rdbuf();
            cout.rdbuf(outputFile.rdbuf());
            Sp1.data_double[i].display();
            cout.rdbuf(originalCoutStreamBuffer);
            outputFile << "plot(x,y_" << N1 << "_" << i << ",'" << color[k] << "'"
                       << ");" << endl;
            outputFile << "hold on;" << endl;
        }
        outputFile << endl;

        // 计算maxnorm
        double max = 0;
        for (i = 0; i < N1 - 1; i++)
        {
            double xx = points[i] + 1.0 / (N1 - 1);
            double tmpd = abs(f(xx) - Sp1.data_double[i](xx));
            if (tmpd > max)
            {
                max = tmpd;
            }
        }
        maxnorm[0][k] = max;
    }

    for (i = 0; i < 10; i++)
    {
        outputFile << endl;
    }

    // nature --------------------------------------------
    outputFile << "x=-1:0.001:1;" << endl;
    outputFile << "f=1.0./(1 + 25 .* x .* x);" << endl;
    outputFile << "plot(x,f,'k');" << endl;
    outputFile << "hold on;" << endl;

    for (k = 0; k < 5; k++)
    {
        int N1 = inputN[k];
        outputFile << "% N = " << N1 << ", Type = nature" << endl;
        vector<double> points(N1, 0);
        vector<vector<double>> F(2, vector<double>(N1, 0));
        for (i = 0; i < N1; i++)
        {
            points[i] = -1 + 2.0 / (N1 - 1) * i;
            F[0][i] = f(points[i]);
        }
        F[1][0] = 50.0 / 26 / 26;
        F[1][N1 - 1] = -50.0 / 26 / 26;
        vector<int> M(N1, 0);
        InterpConditions condition = {N1, points, M, F};
        Spline<1, 3, ppForm> Sp1(N1);
        Sp1 = interpolate_ppForm<3>(condition, nature);
        for (i = 0; i < N1 - 1; i++)
        {
            outputFile << "x=" << points[i] << ":0.001:" << points[i + 1] << ";" << endl;
            outputFile << "y_" << N1 << "_" << i << "=";
            streambuf *originalCoutStreamBuffer = cout.rdbuf();
            cout.rdbuf(outputFile.rdbuf());
            Sp1.data_double[i].display();
            cout.rdbuf(originalCoutStreamBuffer);
            outputFile << "plot(x,y_" << N1 << "_" << i << ",'" << color[k] << "'"
                       << ");" << endl;
            outputFile << "hold on;" << endl;
        }
        outputFile << endl;

        // 计算maxnorm
        double max = 0;
        for (i = 0; i < N1 - 1; i++)
        {
            double xx = points[i] + 1.0 / (N1 - 1);
            double tmpd = abs(f(xx) - Sp1.data_double[i](xx));
            if (tmpd > max)
            {
                max = tmpd;
            }
        }
        maxnorm[1][k] = max;
    }

    for (i = 0; i < 10; i++)
    {
        outputFile << endl;
    }

    // Bspline --------------------------------------------
    outputFile << "x=-1:0.001:1;" << endl;
    outputFile << "f=1.0./(1 + 25 .* x .* x);" << endl;
    outputFile << "plot(x,f,'k');" << endl;
    outputFile << "hold on;" << endl;

    for (k = 0; k < 5; k++)
    {
        int N1 = inputN[k];
        outputFile << "% N = " << N1 << ", Type = Bspline" << endl;
        vector<double> points(N1, 0);
        vector<vector<double>> F(2, vector<double>(N1, 0));
        for (i = 0; i < N1; i++)
        {
            points[i] = -1 + 2.0 / (N1 - 1) * i;
            F[0][i] = f(points[i]);
        }
        F[1][0] = 50.0 / 26 / 26;
        F[1][N1 - 1] = -50.0 / 26 / 26;
        vector<int> M(N1, 0);
        InterpConditions condition = {N1, points, M, F};
        int Controln = N1 + 3 - 3;
        vector<double> controlpoints(Controln, 0);
        vector<double> controlvalue(Controln, 0);
        for (i = 0; i < Controln; i++)
        {
            controlpoints[i] = -1 + 2.0 / (Controln - 1) * i;
            controlvalue[i] = f(controlpoints[i]);
        }
        Controlpoint control = {controlpoints, controlvalue};
        Spline<1, 3, BForm> Sp1(N1);
        Sp1 = interpolate_BForm<3>(condition, control);
        for (i = 0; i < N1 - 1; i++)
        {
            outputFile << "x=" << points[i] << ":0.001:" << points[i + 1] << ";" << endl;
            outputFile << "y_" << N1 << "_" << i << "=";
            streambuf *originalCoutStreamBuffer = cout.rdbuf();
            cout.rdbuf(outputFile.rdbuf());
            Sp1.data_double[i].display();
            cout.rdbuf(originalCoutStreamBuffer);
            outputFile << "plot(x,y_" << N1 << "_" << i << ",'" << color[k] << "'"
                       << ");" << endl;
            outputFile << "hold on;" << endl;
        }
        outputFile << endl;

        // 计算maxnorm
        double max = 0;
        for (i = 0; i < N1 - 1; i++)
        {
            double xx = points[i] + 1.0 / (N1 - 1);
            double tmpd = abs(f(xx) - Sp1.data_double[i](xx));
            if (tmpd > max)
            {
                max = tmpd;
            }
        }
        maxnorm[2][k] = max;
    }

    for (i = 0; i < 10; i++)
    {
        outputFile << endl;
    }
    // linear --------------------------------------------
    outputFile << "x=-1:0.001:1;" << endl;
    outputFile << "f=1.0./(1 + 25 .* x .* x);" << endl;
    outputFile << "plot(x,f,'k');" << endl;
    outputFile << "hold on;" << endl;

    for (k = 0; k < 5; k++)
    {
        int N1 = inputN[k];
        outputFile << "% N = " << N1 << ", Type = linear" << endl;
        vector<double> points(N1, 0);
        vector<vector<double>> F(2, vector<double>(N1, 0));
        for (i = 0; i < N1; i++)
        {
            points[i] = -1 + 2.0 / (N1 - 1) * i;
            F[0][i] = f(points[i]);
        }
        F[1][0] = 50.0 / 26 / 26;
        F[1][N1 - 1] = -50.0 / 26 / 26;
        vector<int> M(N1, 0);
        InterpConditions condition = {N1, points, M, F};
        Spline<1, 1, ppForm> Sp1(N1);
        Sp1 = interpolate_ppForm<1>(condition, complete);
        for (i = 0; i < N1 - 1; i++)
        {
            outputFile << "x=" << points[i] << ":0.001:" << points[i + 1] << ";" << endl;
            outputFile << "y_" << N1 << "_" << i << "=";
            streambuf *originalCoutStreamBuffer = cout.rdbuf();
            cout.rdbuf(outputFile.rdbuf());
            Sp1.data_double[i].display();
            cout.rdbuf(originalCoutStreamBuffer);
            outputFile << "plot(x,y_" << N1 << "_" << i << ",'" << color[k] << "'"
                       << ");" << endl;
            outputFile << "hold on;" << endl;
        }
        outputFile << endl;

        // 计算maxnorm
        double max = 0;
        for (i = 0; i < N1 - 1; i++)
        {
            double xx = points[i] + 1.0 / (N1 - 1);
            double tmpd = abs(f(xx) - Sp1.data_double[i](xx));
            if (tmpd > max)
            {
                max = tmpd;
            }
        }
        maxnorm[3][k] = max;
    }


    outputFile << "maxnorm:" << endl;
    outputFile << "               6            11            21             41              81" << endl;
    outputFile << "complete: ";
    for (i = 0; i < 5; i++)
    {
        outputFile << "     " << maxnorm[0][i];
    }
    outputFile << endl;
    outputFile << "nature: ";
    for (i = 0; i < 5; i++)
    {
        outputFile << "     " << maxnorm[1][i];
    }
    outputFile << endl;
    outputFile << "Bspline: ";
    for (i = 0; i < 5; i++)
    {
        outputFile << "     " << maxnorm[2][i];
    }
    outputFile << endl;
    outputFile << "linear: ";
        for (i = 0; i < 5; i++)
    {
        outputFile << "     " << maxnorm[3][i];
    }
    outputFile << endl;

    outputFile.close();

    return 0;
}