#include "src/Spline.h"

double f(double x)
{
    return 1.0 / (1 + x * x);
}

int main()
{

    int i, j, k;

    std::ofstream outputFile("questionC.txt");

    outputFile << "x=-5:0.001:5;" << endl;
    outputFile << "f=1./(1+x.*x);" << endl;
    outputFile << "plot(x,f,'k');" << endl;
    outputFile << "hold on;" << endl;

    int n1 = 11;
    vector<double> point1(n1);
    vector<int> M1(n1);
    vector<vector<double>> F1(2, vector<double>(n1, 0));
    for (i = 0; i < n1; i++)
    {
        M1[i] = 0;
        point1[i] = -6 + i + 1;
        F1[0][i] = f(point1[i]);
    }
    F1[1][0] = 10.0 / 26 / 26;
    F1[1][n1 - 1] = -10.0 / 26 / 26;
    struct InterpConditions condition1 = {n1, point1, M1, F1};
    Spline<1, 3, cardinalB> Sp1(n1);
    Sp1 = interpolate_cardinalB<3>(condition1);
    for (i = 0; i < n1 - 1; i++)
    {
        outputFile << "x=" << point1[i] << ":0.001:" << point1[i + 1] << ";" << endl;
        outputFile << "y_" << n1 << "_" << i << "=";
        streambuf *originalCoutStreamBuffer = cout.rdbuf();
        cout.rdbuf(outputFile.rdbuf());
        Sp1.data_double[i].display();
        cout.rdbuf(originalCoutStreamBuffer);
        outputFile << "plot(x,y_" << n1 << "_" << i << ",'r'"
                   << ");" << endl;
        outputFile << "hold on;" << endl;
    }

    int n2 = 12;
    vector<double> point2(n2);
    vector<int> M2(n2);
    vector<vector<double>> F2(2, vector<double>(n2, 0));
    for (i = 0; i < n2; i++)
    {
        M2[i] = 0;
    }
    point2[0] = -5;
    F2[0][0] = f(point2[0]);
    for (i = 1; i <= n2 - 2; i++)
    {
        point2[i] = i - 11.0 / 2;
        F2[0][i] = f(point2[i]);
    }
    point2[n2 - 1] = 5;
    F2[0][n2 - 1] = f(point2[n2 - 1]);
    struct InterpConditions condition2 = {n2, point2, M2, F2};
    Spline<1, 2, cardinalB> Sp2(n2 - 1);
    Sp2 = interpolate_cardinalB<2>(condition2);
    for (i = 0; i < n2 - 2; i++)
    {
        outputFile << "x=" << point1[i] << ":0.001:" << point1[i + 1] << ";" << endl;
        outputFile << "y_" << n2 << "_" << i << "=";
        streambuf *originalCoutStreamBuffer = cout.rdbuf();
        cout.rdbuf(outputFile.rdbuf());
        Sp2.data_double[i].display();
        cout.rdbuf(originalCoutStreamBuffer);
        outputFile << "plot(x,y_" << n2 << "_" << i << ",'b'"
                   << ");" << endl;
        outputFile << "hold on;" << endl;
    }

    for (i = 0; i < 5; i++)
    {
        outputFile << endl;
    }

    // question D

    outputFile << "quadratic cardinal B-splines: " << endl;
    outputFile << "x = -3.5, E_S(x) = " << abs(Sp1.data_double[1](-3.5) - f(-3.5)) << endl;
    outputFile << "x = -3, E_S(x) = " << abs(Sp1.data_double[1](-3) - f(-3)) << endl;
    outputFile << "x = -0.5, E_S(x) = " << abs(Sp1.data_double[4](-0.5) - f(-0.5)) << endl;
    outputFile << "x = 0, E_S(x) = " << abs(Sp1.data_double[4](0) - f(0)) << endl;
    outputFile << "x = 0.5, E_S(x) = " << abs(Sp1.data_double[5](0.5) - f(0.5)) << endl;
    outputFile << "x = 3, E_S(x) = " << abs(Sp1.data_double[8](3) - f(3)) << endl;
    outputFile << "x = 3.5, E_S(x) = " << abs(Sp1.data_double[8](3.5) - f(3.5)) << endl;

    outputFile << endl;

    outputFile << "cubic cardinal B-splines: " << endl;
    outputFile << "x = -3.5, E_S(x) = " << abs(Sp2.data_double[1](-3.5) - f(-3.5)) << endl;
    outputFile << "x = -3, E_S(x) = " << abs(Sp2.data_double[1](-3) - f(-3)) << endl;
    outputFile << "x = -0.5, E_S(x) = " << abs(Sp2.data_double[4](-0.5) - f(-0.5)) << endl;
    outputFile << "x = 0, E_S(x) = " << abs(Sp2.data_double[4](0) - f(0)) << endl;
    outputFile << "x = 0.5, E_S(x) = " << abs(Sp2.data_double[5](0.5) - f(0.5)) << endl;
    outputFile << "x = 3, E_S(x) = " << abs(Sp2.data_double[8](3) - f(3)) << endl;
    outputFile << "x = 3.5, E_S(x) = " << abs(Sp2.data_double[8](3.5) - f(3.5)) << endl;

    outputFile.close();

    return 0;
}
