#include "src/Spline.h"

using namespace std;

double f_upper(double x)
{
    return 2.0 / 3 * (sqrt(abs(x)) + sqrt(abs(3 - x * x)));
}

double f_below(double x)
{
    return 2.0 / 3 * (sqrt(abs(x)) - sqrt(abs(3 - x * x)));
}
int main()
{
    std::ofstream outputFile("questionE.txt");
    char color[3] = {'y', 'r', 'g'};
    vector<int> n = {10, 40, 160};
    for (int nn = 0; nn < 3; nn++)
    {
        int u = n[nn]/4;
        int v = n[nn]%4;
        vector<vector<vector<double>>> input(2);
        vector<vector<vector<double>>> output(2);
        for(int i = 0; i < u + v/2; i++)
        {
            vector<double> a;
            a.push_back(-i*sqrt(3)/(u+v/2));
            a.push_back(f_upper(-i*sqrt(3)/(u+v/2)));
            input[0].push_back(a);
        }
        for(int i = 0; i < u + v/2; i++)
        {
            vector<double> a;
            a.push_back(sqrt(3)*i/(u+v/2));
            a.push_back(f_upper(sqrt(3)*i/(u+v/2)));
            input[1].push_back(a);
        }
        for(int i = 0; i < u + 1; i++)
        {
            vector<double> a;
            a.push_back(-sqrt(3)+i*sqrt(3)/u);
            a.push_back(f_below(-sqrt(3)+i*sqrt(3)/u));
            input[0].push_back(a);
        }
        for(int i = 0; i < u + 1; i++)
        {
            vector<double> a;
            a.push_back(sqrt(3)-i*sqrt(3)/u);
            a.push_back(f_below(sqrt(3)-i*sqrt(3)/u));
            input[1].push_back(a);
        }
        output[0] = fitCurve<2,3>(input[0]);
        output[1] = fitCurve<2,3>(input[1]);
        
        outputFile << "%n = " << n[nn] << ":" << endl;
        for (int j = 0; j < 2; j++)
        {
            outputFile << "x" << j+1 << " = [";
            for(int i = 0; i < 1000; i++)
            {
                outputFile  << output[j][0][i] << '\t' ;
            }
            outputFile << "];" << endl;

            outputFile << "y" << j+1 << " = [";
            for(int i = 0; i < 1000; i++)
            {
                outputFile  << output[j][1][i] << '\t' ;
            }
            outputFile << "];" << endl;
            outputFile << "plot(x" << j+1 << ",y" << j+1 << ",'" << color[nn] << "'" << ");" << endl;
            outputFile << "hold on;" << endl;
            outputFile << endl;
        }
    }
    outputFile.close();
    return 0;
}