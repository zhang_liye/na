#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include <stdio.h>
#include <lapacke.h>
using namespace std;

template <int Order> // 多项式类
class Polynomial
{
private:
    vector<double> polynomial; // 多项式的系数
    int n;                       // 多项式的系数个数
public:
    Polynomial() // 构造函数
    {
        n = Order + 1;
        polynomial.resize(n);
    }
    Polynomial(int nn) // 构造函数
    {
        n = Order + 1;
        polynomial.resize(n);
    }
    Polynomial(const Polynomial &p) : polynomial(p.polynomial), n(p.n) {}     // 拷贝构造函数
    Polynomial(const vector<double> &_polynomial) : polynomial(_polynomial) // vector初始化
    {
        n = Order + 1;
        polynomial.resize(n);
    }
    double operator()(double x) // 计算多项式值
    {
        double sum = 0;
        for (int i = 0; i < n; ++i)
        {
            double t = pow(x, i);
            double tt = polynomial[i] * t;
            sum = sum + tt;
        }
        return sum;
    }
    Polynomial derivation() // 计算多项式导数
    {
        Polynomial aa(polynomial);
        for (int i = 0; i <= Order; ++i)
        {
            aa.polynomial[i] = aa.polynomial[i + 1] * (i + 1);
        }
        aa.polynomial[Order] -= aa.polynomial[Order];
        return aa;
    }
    void display() // 输出多项式表达式
    {
        for (int i = 0; i <= Order; i++)
        {
            cout << "+(" << polynomial[i] << ").*x.^(" << i << ")";
        }
        if (n == 0)
        {
            cout << "0";
        }
        cout << ";";
        cout << endl;
    }
    Polynomial<Order> &operator=(Polynomial<Order> c) // 多项式赋值
    {
        for (int i = 0; i <= Order; ++i)
        {
            this->polynomial[i] = c.polynomial[i];
        }
        return *this;
    }
    template <int Order1, int Order2> // 加法操作
    friend Polynomial<(Order1 > Order2 ? Order1 : Order2)> operator+(Polynomial<Order1> &c1, Polynomial<Order2> &c2);

    template <int Order1, int Order2> // 减法操作
    friend Polynomial<(Order1 > Order2 ? Order1 : Order2)> operator-(Polynomial<Order1> &c1, Polynomial<Order2> &c2);

    template <int Order1> // 乘法操作
    friend Polynomial<Order1> operator*(Polynomial<Order1> &c1, Polynomial<Order1> &c2);

    template <int Order1> // 数乘操作
    friend Polynomial<Order1> operator*(double k, Polynomial<Order1> &c1);
};

template <int Order1, int Order2>
Polynomial<(Order1 > Order2 ? Order1 : Order2)> operator+(Polynomial<Order1> &c1, Polynomial<Order2> &c2) // 多项式求和
{
    const int n = (Order1 > Order2 ? Order1 : Order2);
    Polynomial<n> t(0);
    if (Order1 > Order2)
    {
        for (int i = 0; i <= Order2; ++i)
        {
            t.polynomial[i] = c1.polynomial[i] + c2.polynomial[i];
        }
        for (int i = Order2 + 1; i <= Order1; ++i)
        {
            t.polynomial[i] = c1.polynomial[i];
        }
    }
    else
    {
        for (int i = 0; i <= Order1; ++i)
        {
            t.polynomial[i] = c1.polynomial[i] + c2.polynomial[i];
        }
        for (int i = Order1 + 1; i <= Order2; ++i)
        {
            t.polynomial[i] = c2.polynomial[i];
        }
    }
    return t;
}

template <int Order1, int Order2>
Polynomial<(Order1 > Order2 ? Order1 : Order2)> operator-(Polynomial<Order1> &c1, Polynomial<Order2> &c2)
{
    const int n = (Order1 > Order2 ? Order1 : Order2);
    Polynomial<n> t(0);
    if (Order1 > Order2)
    {
        for (int i = 0; i <= Order2; ++i)
        {
            t.polynomial[i] = c1.polynomial[i] - c2.polynomial[i];
        }
        for (int i = Order2 + 1; i <= Order1; ++i)
        {
            t.polynomial[i] = c1.polynomial[i];
        }
    }
    else
    {
        for (int i = 0; i <= Order1; ++i)
        {
            t.polynomial[i] = c1.polynomial[i] - c2.polynomial[i];
        }
        for (int i = Order1 + 1; i <= Order2; ++i)
        {
            t.polynomial[i] = c2.polynomial[i];
        }
    }
    return t;
}
template <int Order1>
Polynomial<Order1> operator*(Polynomial<Order1> &c1, Polynomial<Order1> &c2) // 多项式乘法
{
    const int n = Order1;
    Polynomial<n> t(0);
    for (int i = 0; i <= n; ++i)
    {
        for (int j = 0; j <= i; ++j)
        {
            t.polynomial[i] += c1.polynomial[j] * c2.polynomial[i - j];
        }
    }
    return t;
}

template <int Order1>
Polynomial<Order1> operator*(double k, Polynomial<Order1> &c1) // 多项式数乘
{
    Polynomial<Order1> t(0);
    for (int i = 0; i <= Order1; ++i)
    {
        t.polynomial[i] = k * c1.polynomial[i];
    }
    return t;
}