#include "Polynomial.h"

class InterpConditions
{
public:
    int num;                  // 插值点的个数
    vector<double> points;    // 插值点
    vector<int> M;            // 插值点对应导数的最高次数
    vector<vector<double>> F; // 插值点对应导数的值
};

enum SplineForm // Spline的类型
{
    ppForm,
    BForm,
    cardinalB
};

enum BCType // 暂时实现complete,cubic,nature,periodic这四种边界条件
{
    complete,
    cubic,
    nature,
    periodic,
};

class Controlpoint
{
public:
    vector<double> point;
    vector<double> value;
};

template <int Dim, int Order, SplineForm t>
class Spline
{
public:
    vector<double> T;                                    // 插值点
    vector<Polynomial<Order>> data_double;               // 插值多项式

    Spline(){};
    Spline(int N) // N表示插值个数
    {
        data_double.resize(N);
    }
    Spline(int N, vector<double> input) // input表示插值点
    {
        data_double.resize(N - 1);
        T = input;
    }
    ~Spline(){};

    template <int Ord>
    friend Spline<1, Order, ppForm> interpolate_ppForm(const InterpConditions &condition, BCType type);

    template <int Ord>
    friend Spline<1, Order, BForm> interpolate_BForm(const InterpConditions &condition, Controlpoint &control);

    template <int Ord>
    friend Spline<1, Order, cardinalB> interpolate_cardinalB(const InterpConditions &condition);

    template <int Ord>
    friend vector<vector<double>> fitCurve(const std::vector<vector<double>> &input);
};

template <int Ord>
Spline<1, Ord, ppForm> interpolate_ppForm(const InterpConditions &condition, BCType type)
{
    int n = condition.num;
    vector<double> x(n);
    vector<double> f(n);
    for (int i = 0; i < n; i++)
    {
        x[i] = condition.points[i];
        f[i] = condition.F[0][i];
    }
    if (Ord == 1) // ppForm Spline S_{0}^{1}
    {
        Spline<1, Ord, ppForm> result(n, x);
        vector<double> x0(Ord + 1, 0);
        for (int i = 1; i < n; i++)
        {
            x0[1] = (f[i] - f[i - 1]) / (x[i] - x[i - 1]);
            x0[0] = f[i] - x0[1] * x[i];
            Polynomial<Ord> tt(x0);
            result.data_double[i - 1] = tt;
        }
        return result;
    }
    else if (Ord == 3) // ppForm Spline S_{2}^{3}
    {
        double m[n][n]; // 用于求插值点一阶导数的矩阵
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                m[i][j] = 0;
            }
        }
        double b[n]; // 用于求插值点一阶导数的向量
        if (type == complete)
        {
            m[0][0] = 2;
            b[0] = 2 * condition.F[1][0];
            for (int i = 1; i < n - 1; i++)
            {
                double lam = (x[i + 1] - x[i]) / (x[i + 1] - x[i - 1]);
                double miu = (x[i] - x[i - 1]) / (x[i + 1] - x[i - 1]);
                m[i][i - 1] = lam;
                m[i][i] = 2;
                m[i][i + 1] = miu;
                b[i] = 3 * lam * (f[i + 1] - f[i]) / (x[i + 1] - x[i]) + 3 * miu * (f[i] - f[i - 1]) / (x[i] - x[i - 1]);
            }
            m[n - 1][n - 1] = 2;
            b[n - 1] = 2 * condition.F[1][n - 1];

            int ipiv[n];

            LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, 1, m[0], n, &ipiv[0], &b[0], 1);
        }
        if (type == cubic)
        {
            m[0][0] = 2;
            m[0][1] = 1;
            b[0] = 3 * (f[1] - f[0]) / (x[1] - x[0]) - (x[1] - x[0]) * condition.F[2][0] / 2;
            for (int i = 1; i < n - 1; i++)
            {
                double lam = (x[i + 1] - x[i]) / (x[i + 1] - x[i - 1]);
                double miu = (x[i] - x[i - 1]) / (x[i + 1] - x[i - 1]);
                m[i][i - 1] = lam;
                m[i][i] = 2;
                m[i][i + 1] = miu;
                b[i] = 3 * lam * (f[i + 1] - f[i]) / (x[i + 1] - x[i]) + 3 * miu * (f[i] - f[i - 1]) / (x[i] - x[i - 1]);
            }
            m[n - 1][n - 2] = 1;
            m[n - 1][n - 1] = 2;
            b[n - 1] = 3 * (f[n - 1] - f[n - 2]) / (x[n - 1] - x[n - 2]) - (x[n - 1] - x[n - 2]) * condition.F[2][n - 1] / 2;

            int ipiv[n];

            LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, 1, m[0], n, &ipiv[0], &b[0], 1);
        }
        if (type == nature)
        {
            m[0][0] = 2;
            m[0][1] = 1;
            b[0] = 3 * (f[1] - f[0]) / (x[1] - x[0]);
            for (int i = 1; i < n - 1; i++)
            {
                double lam = (x[i + 1] - x[i]) / (x[i + 1] - x[i - 1]);
                double miu = (x[i] - x[i - 1]) / (x[i + 1] - x[i - 1]);
                m[i][i - 1] = lam;
                m[i][i] = 2;
                m[i][i + 1] = miu;
                b[i] = 3 * lam * (f[i + 1] - f[i]) / (x[i + 1] - x[i]) + 3 * miu * (f[i] - f[i - 1]) / (x[i] - x[i - 1]);
            }
            m[n - 1][n - 2] = 1;
            m[n - 1][n - 1] = 2;
            b[n - 1] = 3 * (f[n - 1] - f[n - 2]) / (x[n - 1] - x[n - 2]);

            int ipiv[n];

            LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, 1, m[0], n, &ipiv[0], &b[0], 1);
        }
        if (type == periodic)
        {
            m[0][0] = 2;
            m[0][1] = (x[1] - x[0]) / (x[2] - x[0]);
            m[0][n - 1] = (x[2] - x[1]) / (x[2] - x[0]);
            b[0] = 3 * m[0][n - 1] * (f[1] - f[0]) / (x[1] - x[0]) - 3 * m[0][1] * (f[2] - f[1]) / (x[2] - x[1]);
            for (int i = 1; i < n - 1; i++)
            {
                double lam = (x[i + 1] - x[i]) / (x[i + 1] - x[i - 1]);
                double miu = (x[i] - x[i - 1]) / (x[i + 1] - x[i - 1]);
                m[i][i - 1] = lam;
                m[i][i] = 2;
                m[i][i + 1] = miu;
                b[i] = 3 * lam * (f[i + 1] - f[i]) / (x[i + 1] - x[i]) + 3 * miu * (f[i] - f[i - 1]) / (x[i] - x[i - 1]);
            }
            m[n - 1][n - 2] = (x[1] - x[0]) / ((x[n - 1] - x[n - 2]) + (x[1] - x[0]));
            m[n - 1][0] = 1 - m[n - 1][n - 2];
            m[n - 1][n - 1] = 2;
            b[n - 1] = 3 * ((x[n - 1] - x[n - 2]) * (f[1] - f[0]) / (x[1] - x[0]) + (x[1] - x[0]) * (f[n - 1] - f[n - 2]) / (x[n - 1] - x[n - 2])) / ((x[n - 1] - x[n - 2]) + (x[1] - x[0]));

            int ipiv[n];

            LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, 1, m[0], n, &ipiv[0], &b[0], 1);
        }
        if (type != complete && type != cubic && type != nature && type != periodic)
        {
            cout << "====输入的非 complete、cubic、nature、periodic 其中一种====" << endl;
        }
        Spline<1, Ord, ppForm> result(n, x);
        vector<double> x0(Ord + 1, 0);
        for (int i = 0; i < n - 1; i++)
        {
            double c0 = f[i];
            double c1 = b[i];
            double c2 = (3 * (f[i + 1] - f[i]) / (x[i + 1] - x[i]) - 2 * b[i] - b[i + 1]) / (x[i + 1] - x[i]);
            double c3 = (b[i] + b[i + 1] - 2 * (f[i + 1] - f[i]) / (x[i + 1] - x[i])) / pow(x[i + 1] - x[i], 2);
            x0[0] = c0 - c1 * x[i] + c2 * pow(x[i], 2) - c3 * pow(x[i], 3);
            x0[1] = c1 - 2 * c2 * x[i] + 3 * c3 * pow(x[i], 2);
            x0[2] = c2 - 3 * c3 * x[i];
            x0[3] = c3;
            Polynomial<Ord> tt(x0);
            result.data_double[i] = tt;
        }
        return result;
    }
}

template <int Ord>
Spline<1, Ord, BForm> interpolate_BForm(const InterpConditions &condition, Controlpoint &control)
{
    int n = condition.num;
    vector<double> x(n);
    for (int i = 0; i < n; i++)
    {
        x[i] = condition.points[i];
    }
    double a[n + 2 * Ord]; // 样本点扩充
    double distance = (x[n - 1] - x[0]) / (n - 1);
    for (int i = 0; i < Ord; i++)
    {
        a[i] = x[0] - (Ord - i) * distance;
    }
    for (int i = Ord; i < n + Ord; i++)
    {
        a[i] = x[i - Ord];
    }
    for (int i = n + Ord; i < n + 2 * Ord; i++)
    {
        a[i] = x[n - 1] + (i - n - Ord + 1) * distance;
    }

    vector<Spline<1, Ord, BForm>> base; // 基函数

    for (int i = 0; i < n + 2 * Ord - 2; i++)
    {
        Spline<1, Ord, BForm> eachbase(n + 2 * Ord, x);
        vector<double> x0(Ord + 1, 0);
        for (int j = 0; j < n + 2 * Ord - 1; j++)
        {
            if (j == i)
            {
                x0[0] = -a[j] / (a[j + 1] - a[j]);
                x0[1] = 1 / (a[j + 1] - a[j]);
            }
            else if (j == i + 1)
            {
                x0[0] = a[j + 1] / (a[j + 1] - a[j]);
                x0[1] = -1 / (a[j + 1] - a[j]);
            }
            else
            {
                x0[0] = 0;
                x0[1] = 0;
            }
            Polynomial<Ord> tt(x0);
            eachbase.data_double[j] = tt;
        }
        base.push_back(eachbase);
    }
    vector<Spline<1, Ord, BForm>> C(base); // n次基函数
    for (int j = 0; j < Ord - 1; j++)
    {
        vector<Spline<1, Ord, BForm>> B;
        for (int i = 0; i < n + 2 * Ord - 3 - j; i++)
        {
            Spline<1, Ord, BForm> eachB(n + 2 * Ord, x);
            vector<double> x0(Ord + 1, 0);
            vector<double> x1(Ord + 1, 0);
            x0[0] = -a[i] / (a[i + 2 + j] - a[i]);
            x0[1] = 1 / (a[i + 2 + j] - a[i]);
            x1[0] = a[i + 3 + j] / (a[i + 3 + j] - a[i + 1]);
            x1[1] = -1 / (a[i + 3 + j] - a[i + 1]);
            Polynomial<Ord> tt(x0);
            Polynomial<Ord> ttt(x1);
            for (int k = 0; k < n + 2 * Ord - 1; k++)
            {
                Polynomial<Ord> temp1;
                temp1 = C[i].data_double[k] * tt;
                Polynomial<Ord> temp2;
                temp2 = C[i + 1].data_double[k] * ttt;
                eachB.data_double[k] = temp1 + eachB.data_double[k];
                eachB.data_double[k] = temp2 + eachB.data_double[k];
            }
            B.push_back(eachB);
        }
        C = B;
    }

    vector<double> p(n + Ord - 1);
    vector<double> f(n + Ord - 1);
    for (int i = 0; i < n + Ord - 3; i++)
    {
        p[i] = control.point[i];
        f[i] = control.value[i];
    }

    double m[n + Ord - 1][n + Ord - 1]; // 系数求解
    double b[n + Ord - 1];
    for (int i = 0; i < n + Ord - 1; i++)
    {
        if (i == 0)
        {
            int t = 0;
            while (p[i] > a[t + 1])
            {
                t += 1;
            }
            for (int j = 0; j < n + Ord - 1; j++)
            {
                m[i][j] = C[j].data_double[t].derivation()(p[i]);
            }
            b[i] = condition.F[1][0];
        }
        else if (i == n + Ord - 2)
        {
            int t = 0;
            while (p[i - 2] > a[t + 1])
            {
                t += 1;
            }
            for (int j = 0; j < n + Ord - 1; j++)
            {
                m[i][j] = C[j].data_double[t].derivation()(p[i-2]);
            }
            b[i] = condition.F[1][n-1];
        }
        else
        {
            int t = 0;
            while (p[i - 1] > a[t + 1])
            {
                t += 1;
            }
            for (int j = 0; j < n + Ord - 1; j++)
            {
                m[i][j] = C[j].data_double[t](p[i-1]);
            }
            b[i] = f[i-1];
        }
    }

    int ipiv[n + Ord - 1];

    LAPACKE_dgesv(LAPACK_ROW_MAJOR, n + Ord - 1, 1, m[0], n + Ord - 1, &ipiv[0], &b[0], 1);

    Spline<1, Ord, BForm> result_ori(n + 2 * Ord, x);
    for (int i = 0; i < n + Ord - 1; i++)
    {
        for (int k = 0; k < n + 2 * Ord - 1; k++)
        {
            Polynomial<Ord> temp;
            temp = b[i] * C[i].data_double[k];
            result_ori.data_double[k] = result_ori.data_double[k] + temp;
        }
    }
    Spline<1, Ord, BForm> result(n, x);
    for (int i = 0; i < n - 1; i++)
    {
        Polynomial<Ord> temp;
        result.data_double[i] = result_ori.data_double[i + Ord] + temp;
    }

    return result;
}

template <int Ord>
Spline<1, Ord, cardinalB> interpolate_cardinalB(const InterpConditions &condition)
{
    int n = condition.num;
    vector<double> x(n);
    vector<double> f(n);
    for (int i = 0; i < n; i++)
    {
        x[i] = condition.points[i];
        f[i] = condition.F[0][i];
    }
    if (Ord == 3)
    {
        double m[n][n]; // 用于求插值点一阶导数的矩阵
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                m[i][j] = 0;
            }
        }
        double b[n]; // 用于求插值点一阶导数的向量
        m[0][0] = 4;
        m[0][1] = 2;
        for (int i = 1; i <= n - 2; i++)
        {
            m[i][i - 1] = 1;
            m[i][i] = 4;
            m[i][i + 1] = 1;
        }
        m[n - 1][n - 2] = 2;
        m[n - 1][n - 1] = 4;

        b[0] = 6 * f[0] + 2 * condition.F[1][0];
        for (int i = 1; i <= n - 2; i++)
        {
            b[i] = 6 * f[i];
        }
        b[n - 1] = 6 * f[n - 1] - 2 * condition.F[1][n - 1];

        int ipiv[n];

        LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, 1, m[0], n, &ipiv[0], &b[0], 1);

        double a[n + 2];
        for (int i = 0; i <= n + 1; i++)
        {
            a[i] = 0;
        }
        a[0] = b[1] - 2 * condition.F[1][0];
        for (int i = 0; i <= n - 1; i++)
        {
            a[i + 1] = b[i];
        }
        a[n + 1] = b[n - 2] + 2 * condition.F[1][n - 1];

        Spline<1, Ord, cardinalB> result(n, f);
        for (int i = 0; i < n - 1; i++)
        {
            double t_double = 0;
            vector<double> t_vector(Ord + 1, 0);
            Polynomial<Ord> t_poly;
            // 第一段
            t_double = 1.0 / 6;
            t_vector[0] = x[i] + 1;
            t_vector[1] = -1;
            Polynomial<Ord> t1(t_vector);
            Polynomial<Ord> t_result1; // 第一段的多项式
            t_result1 = t_double * t1;
            t_result1 = t_result1 * t1;
            t_result1 = t_result1 * t1;
            t_result1 = a[i] * t_result1;
            result.data_double[i] = result.data_double[i] + t_result1;
            // 第二段
            Polynomial<Ord> t_result2;
            t_vector[0] = x[i] + 2;
            t_vector[1] = -1;
            Polynomial<Ord> t2(t_vector);
            t_vector[0] = -x[i];
            t_vector[1] = 1;
            Polynomial<Ord> t22(t_vector);
            t_vector[0] = 2.0 / 3;
            t_vector[1] = 0;
            Polynomial<Ord> t222(t_vector);
            t_result2 = (-1.0 / 2) * t2;
            t_result2 = t_result2 * t22;
            t_result2 = t_result2 * t22;
            t_result2 = t_result2 + t222;
            t_result2 = a[i + 1] * t_result2;
            result.data_double[i] = result.data_double[i] + t_result2;
            // 第三段
            Polynomial<Ord> t_result3;
            t_vector[0] = -x[i] + 1;
            t_vector[1] = 1;
            Polynomial<Ord> t3(t_vector);
            t_vector[0] = x[i] + 1;
            t_vector[1] = -1;
            Polynomial<Ord> t33(t_vector);
            t_vector[0] = 2.0 / 3;
            t_vector[1] = 0;
            Polynomial<Ord> t333(t_vector);
            t_result3 = (-1.0 / 2) * t3;
            t_result3 = t_result3 * t33;
            t_result3 = t_result3 * t33;
            t_result3 = t_result3 + t333;
            t_result3 = a[i + 2] * t_result3;
            result.data_double[i] = result.data_double[i] + t_result3;
            // 第四段
            t_vector[0] = 1.0 / 6;
            t_vector[1] = 0;
            Polynomial<Ord> t_result4(t_vector);
            t_vector[0] = -x[i];
            t_vector[1] = 1;
            Polynomial<Ord> t4(t_vector);
            t_result4 = t_result4 * t4;
            t_result4 = t_result4 * t4;
            t_result4 = t_result4 * t4;
            t_result4 = a[i + 3] * t_result4;
            result.data_double[i] = result.data_double[i] + t_result4;
        }
        return result;
    }
    if (Ord == 2)
    {
        n = n - 1;

        double m[n - 1][n - 1];
        double b[n - 1];
        for (int i = 0; i < n - 1; i++)
        {
            b[i] = 0;
            for (int j = 0; j < n - 1; j++)
            {
                m[i][j] = 0;
            }
        }
        int ipiv[n - 1];

        m[0][0] = 5;
        m[0][1] = 1;
        for (int i = 1; i <= n - 2; i++)
        {
            m[i][i - 1] = 1;
            m[i][i] = 6;
            m[i][i + 1] = 1;
        }
        m[n - 2][n - 3] = 1;
        m[n - 2][n - 2] = 5;

        b[0] = 8 * f[1] - 2 * f[0];
        for (int i = 1; i < n - 2; i++)
        {
            b[i] = 8 * f[i + 1];
        }
        b[n - 2] = 8 * f[n - 1] - 2 * f[n];

        LAPACKE_dgesv(LAPACK_ROW_MAJOR, n - 1, 1, m[0], n - 1, &ipiv[0], &b[0], 1);

        double a[n + 1];
        a[0] = 2 * f[0] - b[0];
        for (int i = 0; i < n - 1; i++)
        {
            a[i + 1] = b[i];
        }
        a[n] = 2 * f[n] - b[n - 2];

        vector<double> input_x(n);
        for (int i = 0; i < n; i++)
        {
            input_x[i] = x[0] + i;
        }

        Spline<1, Ord, cardinalB> result(n, input_x);
        for (int i = 0; i < n - 1; ++i)
        {
            vector<double> t_vector(Ord + 1, 0);
            double t_double;
            // 第一段
            t_vector[0] = 1.0 / 2;
            t_vector[1] = 0;
            Polynomial<Ord> result1(t_vector);
            t_vector[0] = input_x[i] + 1;
            t_vector[1] = -1;
            Polynomial<Ord> t1(t_vector);
            result1 = result1 * t1;
            result1 = result1 * t1;
            result1 = a[i] * result1;
            result.data_double[i] = result.data_double[i] + result1;
            // 第二段
            Polynomial<Ord> result2;
            t_vector[0] = -input_x[i] - 1.0 / 2;
            t_vector[1] = 1;
            Polynomial<Ord> t2(t_vector);
            t_vector[0] = 3.0 / 4;
            t_vector[1] = 0;
            Polynomial<Ord> t22(t_vector);
            result2 = t2;
            result2 = result2 * t2;
            result2 = t22 - result2;
            result2 = a[i + 1] * result2;
            result.data_double[i] = result.data_double[i] + result2;
            // 第三段
            t_vector[0] = 1.0 / 2;
            t_vector[1] = 0;
            Polynomial<Ord> result3(t_vector);
            t_vector[0] = -input_x[i];
            t_vector[1] = 1;
            Polynomial<Ord> t3(t_vector);
            result3 = result3 * t3;
            result3 = result3 * t3;
            result3 = a[i + 2] * result3;
            result.data_double[i] = result.data_double[i] + result3;
        }
        return result;
    }
}

template <int Dim, int Ord>
vector<vector<double>> fitCurve(const vector<vector<double>> &input)
{
    int n = input.size();
    vector<vector<double>> aa(Dim);
    for (int i = 0; i < Dim; i++)
    {
        aa[i].resize(1000);
    }
    vector<double> x(n,0);
    for (int i = 1; i < n; i++)
    {
        for (int j = 0; j < Dim - 1; j++)
        {
            x[i] += pow(input[i][j]-input[i-1][j],2);
        }
        x[i] = sqrt(x[i]) + x[i-1];
    }

    for (int j = 0; j < Dim; j++)
    {
        vector<double> f(n,0);
        for (int i = 0; i < n; i++)
        {
            f[i] = input[i][j];
        }
        double M[n][n];
        for(int i = 0; i < n; i++)
        {
            for(int k = 0; k < n; k++)
            {
                M[i][k] = 0;
            }    
        }
        double b[n];
        for(int i = 1; i <= n - 2; i++)
        {
            M[i][i-1] = x[i] - x[i-1];
            M[i][i] = 2 * (x[i+1] - x[i-1]);
            M[i][i+1] = x[i+1] - x[i];
        }
        
        for(int i = 1; i <= n - 2; i++)
        {
            b[i] = 6 * ((f[i+1] - f[i])/(x[i+1] - x[i]) - (f[i] - f[i-1])/(x[i] - x[i-1]));
        }
        
        M[0][0] = x[0] - x[1];
        M[0][1] = x[2] - x[0];
        M[0][2] = x[0] - x[1];
        b[0] = 0;
        
        M[n-1][n-3] = x[n-2] - x[n-1];
        M[n-1][n-2] = x[n-1] - x[n-3];
        M[n-1][n-1] = x[n-3] - x[n-2];
        b[n-1] = 0;

        int ipiv[n];

        LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, 1, M[0], n, &ipiv[0], &b[0], 1);
        
        Spline<1, Ord, ppForm> result(n, x);
        for(int i = 1; i < n; i++)
        { 
            double a1 = f[i-1];
            double a2 = (f[i]-f[i-1])/(x[i] - x[i-1]) - (x[i] - x[i-1]) * b[i-1]/2 - (x[i] - x[i-1]) * (b[i] - b[i-1])/6;
            double a3 = b[i-1]/2;
            double a4 = (b[i] - b[i-1])/(6 * (x[i] - x[i-1]));

            vector<double> tt(4, 0);                                                          
            if(x[i-1] == 0 || x[i-1] == -0){
                tt[0] = 0;
            }
            else{
                tt[0] = -x[i-1];
            }
            tt[1] = 1;
            tt[2] = 0;
            tt[3] = 0;
            Polynomial<3> x0(tt);                                     
            vector<double> ttt(4,0);
            ttt[0] = a1;
            Polynomial<3> tttt(ttt);
            Polynomial<3> tmp(0);

            result.data_double[i - 1] = tttt;
            tmp = a2 * x0;
            result.data_double[i - 1] = result.data_double[i - 1] + tmp;
            tmp = a3 * x0;
            tmp = tmp * x0;
            result.data_double[i - 1] = result.data_double[i - 1] + tmp;
            tmp = a4 * x0;
            tmp = tmp * x0;
            tmp = tmp * x0;
            result.data_double[i - 1] = result.data_double[i - 1] + tmp;
        }
        
        for (int i = 0; i < 1000; i++)
        {
            double s = i * x[n-1] / 1000;
            int t = 0;
            while (s < x[t] || s > x[t+1])
            {
                t += 1;
            }
            aa[j][i] = result.data_double[t](s);
        }
    }
    return aa;
}